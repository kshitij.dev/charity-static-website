# 🌟 Static Website for a Charity 🌟

Website developed as a digital platform to showcase the mission, services, and history of the NAB Lions Home for Aging Blind.

Deployed version can be found [here](https://charity-website-kmg.vercel.app/).


## 🌐 Tech Stack
- 💻 jQuery
- 🎨 anime.js
- 🔧 Handlebars
- 🌐 Vanilla JS, CSS, and HTML
- 🖌️ Figma for mockups and UI design

## 📄 License

This project is licensed under the GPL3 License - see the LICENSE file for details.

## 🎨 Design

During discussions with the charity, it was clear that the primary information seekers are high-ranking corporate employees. This is due to Corporate Social Responsibility (CSR) funding initiatives, which require corporations to donate a part of their profits.

Thus, the website is primarily aimed at **corporate sponsors**. Key design features include:

* 📱 **Mobile first:** Optimized for corporate decision-makers who often use smartphones.
* ✒️ **Typography:** Clean, readable, and professional font - Noto Sans.
* 🎨 **Color scheme:** Blue-white palette with red for the call-to-action donate button.
* 🖥️ **Familiarity:** Intuitive navigation and layout similar to well-designed modern websites like Children in Need, Oxfam, and Red Cross.

The website also caters to:
* 💖 Individual donors: The "empower us" page includes a shop and purchase form.
* 🏡 Residents: The updates page shares their experiences and achievements.
* 🤝 Caregivers and volunteers: The FAQ section on the contact us page provides information and a way to get in touch.
* 🧳 Visitors: The reach us section provides essential information.

## 🗂️ Structure

### General

All pages feature a full-screen hero or banner. The home page uses a video hero. This design encourages users to scroll further. A call-to-action button and accessibility menu are included on all pages except the empower page.

### Navigation

Inspired by the Apple Human Interface Design Guide, the navigation ensures users know where they are and can easily find their next stop. The nav bar hides when users scroll down and reappears when they scroll up. A scroll-to-top button is always available.

### Layouts

Responsive, mobile-first layouts with a typical header-main-footer structure:

* 📰 **Missions (Home) and resident interviews (Updates):** Zig-zag pattern.
* 🖼️ **Testimonials (Home):** Auto-playing slideshow.
* 📅 **Events (Updates), leadership (About), and shopping items (Empower Us):** Grid layout.
* 📄 **Facts (About), latest (Home), and 'why donate' (Empower Us):** Side-by-side layout with highlighted keywords.
* 🕰️ **Former patrons-in-chief (About):** Timeline layout.
* 📚 **Footer:** Grid layout with links to every article, social media links, and accessibility menu.

## 💡 Inspirations

### The Apple Human Interface Guidelines

Inspired by Apple's WWDC17 talk and their HIG guidelines:

* 🧼 **Clarity and Simplicity:** Plenty of white space and clear structure.
* 🔄 **Consistency and Familiarity:** Consistent styling and navigation.
* 📱 **Responsive Design:** Mobile-first design.
* ♿ **Accessibility:** Efforts include alt text, keyboard navigation, and proper semantic markup.

### Elise Roy

Elise Roy's TED Talk "When we design for disability, we all benefit" emphasizes the importance of inclusive design. Her message inspired the accessibility-first approach of this project.

### The Residents at the NAB Home

The residents' stories and experiences were a driving force behind the website. Their enthusiasm and support were invaluable, highlighting the importance of inclusion and accessibility in web design.

## ♿ Accessibility

### Accessibility Options

A dedicated drop-down menu offers:

* 🖤 **Grayscale mode:** For users with visual impairments.
* 🔍 **Increased font size:** For improved readability.
* 📝 **Font family switch:** For users with dyslexia or visual processing disorders.
* 🔗 **Underlined links:** For better navigation clarity.

### Apple HIG Inspired Navigation System

Features include a scroll-to-top button, site map in the footer, and clear navigation that always indicates the user's location.

### Support for Assistive Technologies

* 🖼️ **Alt text:** For all images and non-text elements.
* 🔊 **ARIA labels:** For interactive elements.
* 📝 **Form input types:** For better accessibility and usability.
* 🔗 **Hyperlink types:** For semantic meaning and usability across devices.

## 👨‍💻 Usability

### Layout

Following the Gutenberg Diagram, important content is placed in the top-left quadrant. Whitespace is used to reduce cognitive load.

### Call-to-Action Placement

Fitts's Law and the Gestalt principle of proximity guided the placement of the donate button for easy access and visibility.

### Color Scheme

A blue tint reflects the calm personality of the residents, with red used for the urgent CTA button.