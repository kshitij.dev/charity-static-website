/**
 * Connects a click event listener to the burger menu button.
 */
function connectBurgerClickEventListener() {
  // Get the burger menu button element
  const button = $("#hamburger");

  // Return if the button is not found
  if (button.length === 0) return;

  // Add a click event listener to the burger menu button
  button.on("click", function () {
    // Toggle the "open" class on the primary navigation element
    $("#primaryNav").toggleClass("open");

    // Toggle the "burger-menu-close" class on the burger menu button
    button.toggleClass("burger-menu-close");

    // Update the button title based on its current state
    const newTitle = button.attr("title") === "Open Menu" ? "Close Menu" : "Open Menu";
    button.attr("title", newTitle);
  });
}

/**
 * Connects the accessibility menu to the active page.
 * @param {string} pageName - The active page name.
 */
function connectAccessibilityMenuEventListeners(pageName) {
  // Get the html element from the DOM, required to apply data attributes
  const html = $("html");

  // If active page is report.html, set the data attributes to default
  if (pageName === "report") {
    setDataAttributes(html, {
      theme: "default",
      font: "default",
      fontSize: "default",
      underlineLinks: "default",
    });
    return;
  }

  // Get the current settings
  const settings = getCurrentSettings();

  // Set the current settings
  setDataAttributes(html, settings);

  // Event listeners for the buttons
  setupEventListener(html, $("#themeToggle"), settings, "theme", "data-theme");
  setupEventListener(html, $("#fontChange"), settings, "font", "data-font-family");
  setupEventListener(html, $("#fontSizeChange"), settings, "fontSize", "data-font-size");
  setupEventListener(html, $("#underlineLinks"), settings, "underlineLinks", "data-underline-links");
  setupMainAccessibilityToggle($("#accessibilityMenuToggle"));
}

/**
 * Sets up the event listener for the main accessibility menu toggle button.
 * @param {jQuery} accessibilityMenuToggle - The main accessibility menu toggle button.
 */
function setupMainAccessibilityToggle(accessibilityMenuToggle) {
  accessibilityMenuToggle.on("click", () => {
    // If the height is 0, then play the animation to open the menu otherwise close the menu
    if ($(".accessibility-menu").height() === 0) {
      // Animate the menu to open
      anime({
        targets: ".accessibility-menu",
        height: $("#accessibilityMenu")[0].scrollHeight + "px",
        duration: 300,
        easing: "easeInOutQuad",
      });
    } else {
      // Animate the menu to close
      anime({
        targets: ".accessibility-menu",
        height: 0,
        duration: 300,
        easing: "easeInOutQuad",
      });
    }
  });
}

/**
 * Sets up the event listener for a button and updates the settings based on user interaction.
 * @param {jQuery} html - The HTML element.
 * @param {jQuery} button - The button element.
 * @param {Object} settings - The current settings.
 * @param {string} option - The setting option (e.g., "theme", "font", "fontSize").
 * @param {string} attribute - The data attribute to update.
 */
function setupEventListener(html, button, settings, option, attribute) {
  // return if the button is not found
  if (!button) return;

  // Add click event listener to the button
  button.on("click", () => {
    // Determine the new value for the setting based on the current value
    const localStorageValue = settings[option] === "default" ? "accessible" : "default";

    // Update the setting in the local storage
    localStorage.setItem(option, localStorageValue);

    // Update the data attribute on the HTML element
    html.attr(attribute, localStorageValue);

    // Update the setting in the settings object
    settings[option] = localStorageValue;

    // Toggle the button's active state based on the new setting value
    if (settings[option] === "accessible") {
      button.addClass("button-active");
    } else {
      button.removeClass("button-active");
    }
  });

  // When using the keyboard, we want the menu to expand when the user tabs to the button
  button.on("focus", () => {
    if ($(".accessibility-menu").height() === 0) {
      anime({
        targets: ".accessibility-menu",
        height: $("#accessibilityMenu")[0].scrollHeight + "px",
        duration: 300,
        easing: "easeInOutQuad",
      });
    }
  });

  // Set the initial active state of the button based on the current setting value
  if (settings[option] === "accessible") {
    button.addClass("button-active");
  }
}

/**
 * Sets the data attributes on the HTML element.
 * @param {jQuery} html - The HTML element.
 * @param {Object} values - The values to set for the data attributes.
 */
function setDataAttributes(html, values) {
  html.attr("data-theme", values.theme);
  html.attr("data-font-family", values.font);
  html.attr("data-font-size", values.fontSize);
  html.attr("data-underline-links", values.underlineLinks);
}

/**
 * Gets the settings from the local storage.
 * @returns {Object} - The current settings.
 */
function getCurrentSettings() {
  // Get the theme value from local storage
  const theme = localStorage.getItem("theme");

  // Get the font value from local storage
  const font = localStorage.getItem("font");

  // Get the fontSize value from local storage
  const fontSize = localStorage.getItem("fontSize");

  // Get the fontSize value from local storage
  const underlineLinks = localStorage.getItem("underlineLinks");

  // Create a default settings object
  let config = { theme: "default", font: "default", fontSize: "default", underlineLinks: "default" };

  // If theme value exists in local storage, update the config object
  if (theme !== null) {
    config.theme = theme;
  }

  // If font value exists in local storage, update the config object
  if (font !== null) {
    config.font = font;
  }

  // If fontSize value exists in local storage, update the config object
  if (fontSize !== null) {
    config.fontSize = fontSize;
  }

  // If underlineLinks value exists in local storage, update the config object
  if (fontSize !== null) {
    config.underlineLinks = underlineLinks;
  }

  // Return the current settings
  return config;
}

/**
 * Connects a scroll event listener to the navigation element.
 */
function connectNavigationScrollEventListener() {
  const header = $("header");

  // Return if the header is not found
  if (header.length === 0) return;

  // Initialize variables
  let didScroll;
  let previousScrollTop = 0;
  const scrollThreshold = 30;
  const navbarHeight = header.outerHeight();

  // Add a scroll event listener to the window
  $(window).scroll(function () {
    // Set the didScroll variable to true when the user scrolls
    didScroll = true;
  });

  // Set an interval to check if the user has scrolled
  setInterval(function () {
    // Check if the user has scrolled
    if (didScroll) {
      // Call the handleScroll function to handle the scroll event
      handleScroll();

      // Reset the didScroll variable to false
      didScroll = false;
    }
  }, 60);

  /**
   * Nested function which handles the scroll event and
   * adds/removes classes based on scroll direction and position.
   */
  function handleScroll() {
    // Get the current scroll position
    const currentScrollTop = $(window).scrollTop();

    // Do nothing if the scroll distance is within the threshold
    if (Math.abs(previousScrollTop - currentScrollTop) <= scrollThreshold) return;

    // Check if the user is scrolling down and the scroll position is below the navbar
    if (currentScrollTop > previousScrollTop && currentScrollTop > navbarHeight) {
      // Scroll Down
      header.addClass("nav-up");
    } else {
      // Check if the scroll position plus the window height is less than the document height
      if (currentScrollTop + $(window).height() < $(document).height()) {
        // Scroll Up
        header.removeClass("nav-up");
      }
    }

    // Update the previous scroll position
    previousScrollTop = currentScrollTop;
  }
}

/**
 * Defines event listeners for scroll-to-top button.
 */
function connectScrollToTopEventListeners() {
  let button = $("#scrollToTop");

  // Return if the button is not found
  if (button.length === 0) return;

  const scrollThreshold = 500;

  // Scroll event listener to only display the scroll-to-top button when the user has scrolled
  $(window).scroll(function () {
    // Check if the scroll position is greater than the threshold
    if ($(document).scrollTop() > scrollThreshold) {
      // Display the scroll-to-top button
      button.css("display", "block");
    } else {
      // Hide the scroll-to-top button
      button.css("display", "none");
    }
  });

  // Click event listener to scroll to top
  button.on("click", () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  });
}

/**
 * Updates the shopping cart overlay with the number of items in the cart.
 * @param {number} itemsInCart - The number of items in the cart.
 */
function updateShoppingCartOverlay(itemsInCart) {
  // Check if the header exists and if it does, show the header if it is hidden
  const header = $("header");
  if (header.length !== 0) {
    header.removeClass("nav-up");
  }

  const cartOverlay = $(".cart-overlay");

  // Return if the cart overlay is not found
  if (cartOverlay.length === 0) return;

  // Update the text of the cart overlay with the number of items in the cart
  cartOverlay.text(itemsInCart);

  // Store the number of items in the cart in local storage
  localStorage.setItem("itemsInCart", itemsInCart);
}

/**
 * Connects click event listeners to the "Add to Cart" buttons.
 * Updates the shopping cart overlay with the number of items in the cart.
 */
function connectAddToCartClickEventListeners() {
  // Initialize the number of items in the cart
  let itemsInCart = 0;

  // Get the previous number of items in the cart from local storage
  const previousItemsInCart = localStorage.getItem("itemsInCart");

  // If there are previous items in the cart, parse the value and update the itemsInCart variable
  if (previousItemsInCart !== null) {
    itemsInCart = parseInt(previousItemsInCart);

    // Update the shopping cart overlay with the number of items in the cart
    updateShoppingCartOverlay(itemsInCart);
  }

  // Get all elements with the "add-to-cart" class
  const addToCartButtons = $(".add-to-cart");

  // Return if there are no "Add to Cart" buttons
  if (addToCartButtons.length === 0) return;

  // Add a click event listener to all elements with the "add-to-cart" class
  addToCartButtons.on("click", function (event) {
    event.preventDefault();

    // Increment the number of items in the cart
    itemsInCart++;

    // Update the shopping cart overlay with the new number of items
    updateShoppingCartOverlay(itemsInCart);
  });
}

/**
 * Connects a submit event listener to the form element.
 */
function connectFormSubmitEventListener() {
  // Get the form element
  const form = $(".form");

  // Return if the form element is not found
  if (!form.length) return;

  // Add a submit event listener to the form
  form.on("submit", function (event) {
    // Prevent the default form submission behavior
    event.preventDefault();

    // Add animation using anime.js
    anime({
      targets: ".form",
      height: 0,
      duration: 300,
      easing: "easeInOutQuad",
      complete: function () {
        // Replace the form element with a success message. In production website, this would be
        // replaced with a call to the server to submit the form data
        // and do subsequent actions such as email notifications, saving to a database, etc.
        $(".form").replaceWith("<p>Thank you for submitting your message. We will get back to you soon.</p>");
      },
    });
  });
}

/**
 * Connects the header event listeners to the appropriate functions.
 * @param {string} pageName - The active page name.
 */
function connectHeaderEventListeners(pageName) {
  connectBurgerClickEventListener();
  connectNavigationScrollEventListener();
  connectScrollToTopEventListeners();
  connectAccessibilityMenuEventListeners(pageName);
  connectAddToCartClickEventListeners();
}

/**
 * Returns the Handlebars helper object.
 * @returns {Object} - The Handlebars helper object.
 */
function getHandlebarsHelper() {
  return {
    /**
     * Checks if two values are equal.
     * @param {*} v1 - The first value.
     * @param {*} v2 - The second value.
     * @returns {boolean} - True if the values are equal, false otherwise.
     */
    eq: (v1, v2) => v1 === v2,

    /**
     * Checks if two values are not equal.
     * @param {*} v1 - The first value.
     * @param {*} v2 - The second value.
     * @returns {boolean} - True if the values are not equal, false otherwise.
     */
    ne: (v1, v2) => v1 !== v2,

    /**
     * Checks if all arguments are truthy.
     * @returns {boolean} - True if all arguments are truthy, false otherwise.
     */
    and() {
      return Array.prototype.every.call(arguments, Boolean);
    },

    /**
     * Checks if any of the arguments are truthy.
     * @returns {boolean} - True if any of the arguments are truthy, false otherwise.
     */
    or() {
      return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
    },
  };
}

/**
 * Fetches a Handlebars template from the server and compiles it.
 * Inserts the compiled template into the specified element in the DOM.
 * @param {string} templateName - The name of the template file.
 * @param {string} pageName - The active page name.
 */
function fetchAndCompileTemplate(templateName, pageName) {
  // Register the Handlebars helper functions
  Handlebars.registerHelper(getHandlebarsHelper());

  // Fetch the template source from the server
  fetch(`templates/${templateName}.hbs`)
    .then((response) => response.text()) // Extract the response body as a text string
    .then((templateSource) => {
      // Compile the handlebars template
      const template = Handlebars.compile(templateSource);

      // Insert the compiled template into the specified element in the DOM
      document.getElementById(`${templateName}`).innerHTML = template({ pageName });
    })
    .then(() => {
      // Connect event listeners if the template is the header template
      if (templateName === "header") {
        connectHeaderEventListeners(pageName);
      }
    })
    .catch((error) => console.error("Error fetching or compiling template:", error));
}

/**
 * Returns the base name of the current HTML page.
 * @returns {string} - The base name of the current HTML page.
 */
function getPageNameString() {
  // Get the full path of the current HTML page
  let pagePath = window.location.pathname;

  // Split the path by "/"
  let pathParts = pagePath.split("/");

  // Get the last part of the path, which is the file name
  let fileName = pathParts.pop();

  // Remove the file extension from the file name
  let pageName = fileName.replace(".html", "");

  // If the page name is empty, set it to "index"
  pageName = pageName === "" ? "index" : pageName;

  return pageName;
}

/**
 * Click event listeners for the collapsible menu.
 */
function connectCollapsibleClickEventListener() {
  // Get all elements with the class "collapsible"
  const collapsibleButtons = document.getElementsByClassName("collapsible");

  // If there are no collapsible elements, return early
  if (!collapsibleButtons) return;

  // Loop through each collapsible element
  for (let buttonIndex = 0; buttonIndex < collapsibleButtons.length; buttonIndex++) {
    // Add a click event listener to each collapsible element
    collapsibleButtons[buttonIndex].addEventListener("click", function () {
      // Toggle the "collapsible-active" class to expand or collapse the content
      this.classList.toggle("collapsible-active");

      // Get the next sibling element, which is the content to be expanded or collapsed
      const content = this.nextElementSibling;

      // Check if the content element has a max-height style property
      if (content.style.maxHeight) {
        // If it has a max-height, set it to null to collapse the content
        content.style.maxHeight = null;
      } else {
        // If it doesn't have a max-height, set it to the scroll height of the content to expand it
        content.style.maxHeight = content.scrollHeight + "px";
      }
    });
  }
}

/**
 * Recursive function that plays the testimonial carousel by hiding and showing testimonial elements and dot elements.
 * It increments the current testimonial index, resets it if necessary, and sets a timeout to call itself again.
 */
function playTestimonialCarousel() {
  // Get all testimonial elements
  let testimonials = $(".testimonial");

  // If there are no testimonial elements, return without doing anything further
  if (!testimonials) return;

  // Get all dot elements
  let dots = $(".dot");

  // Hide all testimonial elements
  for (let index = 0; index < testimonials.length; index++) {
    $(testimonials[index]).hide();
  }

  // Increment the current testimonial index
  currentTestimonial++;

  // If the current testimonial index exceeds the total number of testimonials, reset it to 1
  if (currentTestimonial > testimonials.length) {
    currentTestimonial = 1;
  }

  // Remove the "active-testimonial" class from all dot elements
  for (let index = 0; index < dots.length; index++) {
    $(dots[index]).removeClass("active-testimonial");
  }

  // Show the current testimonial element
  $(testimonials[currentTestimonial - 1]).show();

  // Add the "active-testimonial" class to the current dot element
  $(dots[currentTestimonial - 1]).addClass("active-testimonial");

  // Set a timeout to call the playTestimonialCarousel function again after 7000 milliseconds
  setTimeout(playTestimonialCarousel, 7000);
}

// Initialize the current testimonial index as a global variable
let currentTestimonial = 0;

/**
 * Fire the main script if the DOM has been loaded (but we don't need to wait for images and videos to load).
 */
$(document).ready(function () {
  // Get the base name of the current HTML page
  let pageName = getPageNameString();

  // Compile the header and footer templates
  fetchAndCompileTemplate("header", pageName);
  fetchAndCompileTemplate("footer", pageName);

  // Connect event listeners that don't depend on the handlebars templates
  connectFormSubmitEventListener();
  connectCollapsibleClickEventListener();

  // Start the testimonial carousel
  playTestimonialCarousel();

  // Start checking for elements in the viewport and animate them ONCE on scroll
  loopContentOnScroll();
});

/* ----------------------------------------------------------------------------
 *                            Show content on scroll
 * ----------------------------------------------------------------------------
 */

/**
 * Adapted version of "Showing images on scroll" - CSS animation tutorial - YouTube [Online]
 * Available from: https://www.youtube.com/watch?v=-ths7kNIFnw.
 */

// Use the requestAnimationFrame method if available, otherwise use a fallback function
// Note, we are using a global variable to simply store the function reference
let showContentOnScrollCallback =
  window.requestAnimationFrame ||
  function (callback) {
    window.setTimeout(callback, 1000 / 60);
  };

/**
 * Checks if an element is within the viewport.
 *
 * @param {Element} element - The element to check.
 * @returns {boolean} Returns true if the element is within the viewport, otherwise false.
 */
function isElementInViewPort(element) {
  const rect = element.getBoundingClientRect();
  return (
    // Check if the top of the element is above or at the top of the viewport
    (rect.top <= 0 && rect.bottom >= 0) ||
    // Check if the bottom of the element is below or at the bottom of the viewport
    (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <= (window.innerHeight || document.documentElement.clientHeight)) ||
    // Check if the element is fully visible within the viewport
    (rect.top >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
  );
}

/**
 * Continuously checks for elements with the class "show-on-scroll" in the viewport and adds the "is-visible" class to them.
 */
function loopContentOnScroll() {
  // Get all elements with the class "show-on-scroll"
  const elementsToShow = document.querySelectorAll(".show-on-scroll");

  // If there are no elements with the class "show-on-scroll", stop the loop
  if (elementsToShow.length === 0) {
    return;
  }

  // Loop through each element
  elementsToShow.forEach(function (element) {
    // Check if the element is in the viewport
    if (isElementInViewPort(element)) {
      // Add the "is-visible" class to the element
      element.classList.add("is-visible");
    }
  });

  // Call the loop function again to continuously check for elements in the viewport
  showContentOnScrollCallback(loopContentOnScroll);
}
